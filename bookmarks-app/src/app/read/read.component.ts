import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { bookmarkModel } from './../models/bookmark.model';
import { AppState } from './../app.state';

import * as BookmarkActions from './../actions/bookmark.actions';

@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.scss']
})
export class ReadComponent implements OnInit {
 
 // We define an observable named bookmarks which we will later display in the template.
  bookmarks: Observable<bookmarkModel[]>;

  // We are accessing the store from ngrx within the constructor, and then selecting 
  //bookmark which is defined as a the property from app.module.ts in StoreModule.forRoot({}). 
  //This calls the bookmark reducer and returns the bookmark state.
  constructor(private store: Store<AppState>) { 
    this.bookmarks = store.select('bookmark');
  }

  ngOnInit() {}

  //Function to delete bookmark

  delBookmark(index) {
    this.store.dispatch(new BookmarkActions.RemoveBookmark(index))
  }
}