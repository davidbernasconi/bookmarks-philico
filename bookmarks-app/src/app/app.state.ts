//importing
import { bookmarkModel } from './models/bookmark.model';

//setup 
export interface AppState {
  readonly bookmark: bookmarkModel[];
}