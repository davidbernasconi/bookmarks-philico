export interface bookmarkModel{
    name: string;
    url: string;
    group: string;
}