// importing our bookmark model and Action from ngrx/store
import { Action } from '@ngrx/store';
import { bookmarkModel } from './../models/bookmark.model';
//Importing all action form bookmark.actions
import * as BookmarkActions from './../actions/bookmark.actions';

//we are defining an initial or default state.
const initialState1: bookmarkModel= {
    name: 'Avaloq',
    url: 'http://avaloq.com',
    group: 'Work'
}

const initialState2: bookmarkModel= {
    name: 'Google',
    url: 'http://google.com',
    group: 'Personal'
}
const initialState3: bookmarkModel= {
    name: 'Finews',
    url: 'http://finews.ch',
    group: 'Leisure'
}

const initialState4: bookmarkModel= {
    name: 'Tio',
    url: 'http://tio.ch',
    group: 'Work'
}

const initialState5: bookmarkModel= {
    name: 'Funny video',
    url: 'https://www.youtube.com/watch?v=9h5mwoTwDBk',
    group: 'Other'
}

// This is the reducer. It takes in a state, which we are defining as a bookmark type and 
//we have optionally bound it to initialState, so that we have an initial data.
//It also takes in the action from our /actions/bookmark.actions file.
export function reducer(state: bookmarkModel[] = [initialState1,
                                                  initialState2,
                                                  initialState3,
                                                  initialState4, 
                                                  initialState5], 
                                                  action: BookmarkActions.Actions) {

//We use a switch to determine the type of the action. 
//In the case of adding a bookmark, we return the new state with the help of our newState() function.
//We're simply passing in the previous state in the first parameter, and then our action in the second.
    switch(action.type) {
        case BookmarkActions.ADD_BOOKMARK:
            return [...state, action.payload];
        // Add this case:
        case BookmarkActions.REMOVE_BOOKMARK:
            state.splice(action.payload, 1)
            return state;
            

        default:
            return state;
    }
}