// importing our bookmark model and Action from ngrx/store
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { bookmarkModel } from './../models/bookmark.model';

// We're defining the type of action, which is in the form of a string constant
export const ADD_BOOKMARK       = '[BOOKMARK] Add';
export const REMOVE_BOOKMARK    = '[BOOKMARK] Remove';

// We are creating a class for each action with a constructor that allows us to pass in the payload.
export class AddBookmark implements Action {
    readonly type = ADD_BOOKMARK

    constructor(public payload: bookmarkModel) {}
}

export class RemoveBookmark implements Action {
    readonly type = REMOVE_BOOKMARK

    constructor(public payload: number) {}
}

// We are exporting all of our action classes for use within our upcoming reducer.
export type Actions = AddBookmark | RemoveBookmark