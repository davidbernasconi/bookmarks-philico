import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from './../app.state';
import { bookmarkModel } from './../models/bookmark.model'
import * as BookmarkActions from './../actions/bookmark.actions';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})

export class CreateComponent implements OnInit {

	name: string;
	url: string;
	group: string;

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
  }

//Function to add new bookmark
  addBookmark(name, url, group) {
    this.store.dispatch(new BookmarkActions.AddBookmark({name: name, url: url, group: group} ))
  }

}
