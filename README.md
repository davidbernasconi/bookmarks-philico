# Bookmarks-philico

Dear User,

save the bookmark-app by running the following code in the terminal in your working directory:
git clone https://davidbernasconi@bitbucket.org/davidbernasconi/bookmarks-philico.git

Then enter in the bookmarks-app folder and run:
npm install

When the installation is complete run the following command:
ng serve -o port 4200 

Open your browser and visit "http://localhost:4200/" there you can test the bookmark app.